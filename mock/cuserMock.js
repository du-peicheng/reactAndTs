module.exports = app => {

  // C端查询
  app.get('/api/cuser/searchData', (req, res) => {
    let cusers = []
    for (let i = 1; i < 100; i++) {
      cusers.push({
            id: i,
            name: '坤坤' + i,
            sculpture: 1,
            type: 'C端用户',
            state: 1
        });
    }
    const search = req.query.data
    cusers = cusers.filter(item => item.id == search || item.name == search)
    res.send({
      code: 0,
      msg: "ok",
      data:{
        total:cusers.length,
        cusers
      }
    })
  })


    // 获取C端用户列表
    app.get('/api/cuser/listdata', (req, res) => {
      let cusersTotal= [];
      for (let i = 1; i < 100; i++) {
        cusersTotal.push({
              id: i,
              name: '坤坤' + i,
              sculpture: 1,
              type: 'C端用户',
              state: 1
          });
      }
      const page = req.query.page ?? 1
      const pagesize = req.query.pagesize ?? 10
      if(req.query.search){
        const  search = req.query.search
        cusers = cusersTotal.filter(item => item.id == search || item.name == search)
      }else{
        cusers = cusersTotal.slice((page-1)*pagesize,page*pagesize)
      }
      res.send({
        code: 0,
        msg: "ok",
        data:{
          total:cusersTotal.length,
          cusers
        }
      })
    })
    // 获取用户信息
    app.get('/api/cuser/detail', (req, res) => {
      let cusersTotal= [];
      for (let i = 1; i < 100; i++) {
        cusersTotal.push({
              id: i,
              name: '坤坤' + i,
              sculpture: 1,
              type: 'C端用户',
              state: Math.random() > 0.5 ? 1 :0
          });
      }
      const page = req.query.page ?? 1
      const pagesize = req.query.pagesize ?? 10
      if(req.query.search){
        const  search = req.query.search
        cusers = cusersTotal.filter(item => item.id == search || item.name == search)
      }else{
        cusers = cusersTotal.slice((page-1)*pagesize,page*pagesize)
      }
      res.send({
        code: 0,
        msg: "ok",
        data:{
          total:cusersTotal.length,
          cusers
        }
      })
    })
    // 状态改变(禁用/启用)
    app.post('/api/cuser/changestate', (req, res) => {
      res.send({
        code: 0,
        msg: "ok",
        data:1
      })
    })
    // 修改c端用户信息
    app.post('/api/cuser/changinfo',(req,res)=>{
      res.send({
        code:0,
        msg:'ok',
        data:1
      })
    })
  }
                                                                                 
