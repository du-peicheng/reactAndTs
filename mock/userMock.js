module.exports = app => {
    // 登录
    app.post('/api/user/login', (req, res) => {
      res.send({
        code: 0,
        msg: "ok",
        data: {
            id:1,
            name:'坤坤',
            token:'rtreghdhdgfhgdfhgdfhdhdfggdgfdhtjkbncvx',
            avatar:'super',
            roleid:1,
            time:Date.now()
        }
      })
    })
    // 返回用户列表
    app.get('/api/user/list',(req,res)=>{
      let usersTotal=[]
      for(let i=1;i<=100;i++){
        usersTotal.push({
          id:i,
          realName:'蔡徐坤',
          name:'kunkun'+i,
          phone:13985274126,
          roleid:Math.random() > 0.5 ? (Math.random() > 0.5 ? 1 : 2) : (Math.random() > 0.5? 3 : 4),
          state: Math.random() > 0.5 ? 1 :0
        })
      }
      const page = req.query.page ?? 1
      const pagesize = req.query.pagesize ?? 10
      users =usersTotal.slice((page-1)*pagesize,page*pagesize)
      res.send({
        code:0,
        msg:'ok',
        data:{
          total:100,
          users
        }
      })
    })
    // 搜索
    app.get('/api/user/search',(req,res)=>{
      const search = req.query.data
      let users=[]
      for(let i=1;i<=100;i++){
        users.push({
          id:i,
          realName:'蔡徐坤',
          name:'kunkun'+i,
          phone:13985274126,
          roleid:Math.random() > 0.5 ? (Math.random() > 0.5 ? 1 : 2) : (Math.random() > 0.5? 3 : 4),
          state: Math.random() > 0.5 ? 1 :0
        })
      }
      users = users.filter(item => item.id == search || item.name == search)
      res.send({
        code:0,
        msg:'ok',
        data:{
          total:users.length,
          users
        }
      })
    })
    // 状态改变
    app.post('/api/user/changestate', (req, res) => {
      res.send({
        code: 0,
        msg: "ok",
        data:1
      })
    })
    // 重置密码
    app.post('/api/user/resetpassword', (req, res) => {
      res.send({
        code: 0,
        msg: "ok",
        data:1
      })
    })
    // 新增用户
    app.post('/api/user/adduser', (req, res) => {
      res.send({
        code: 0,
        msg: "ok",
        data:1
      })
    })
    // 修改用户信息
    app.post('/api/user/changeUser', (req, res) => {
      res.send({
        code: 0,
        msg: "ok",
        data:1
      })
    })
  }