module.exports = app => {
    // 角色列表
    app.get('/api/role/list',(req,res)=>{
        let rolesList = [
            {
                id:1,
                name:'运营专员',
                des:'负责运营',
                count:12,
                node:[1,2,3,4,5]
            },
            {
                id:2,
                name:'订单管理员',
                des:'负责管理订单',
                count:60,
                node:[3,4,5]
            },
            {
                id:3,
                name:'超级管理员',
                des:'霸道总裁和程序员小娇妻',
                count:3,
                node:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
            },
            {
                id:4,
                name:'产品经理',
                des:'他是leader',
                count:5,
                node:[1,2,3,4,5,11,14,15,16]
            },
        ]
      const total=rolesList.length
      const page = req.query.page || 1
      const pagesize = req.query.pagesize ?? 10
      if(page*pagesize>total){
          rolesList =rolesList.slice((page-1)*pagesize,total)
        }else{
            rolesList =rolesList.slice((page-1)*pagesize,page*pagesize)
      }
        res.send({
            code:0,
            msg:'ok',
            data:{
                total,
                rolesList
            }
        })
    })
    // 搜索
    app.get('/api/role/searchList',(req,res)=>{
        let rolesList = [
            {
                id:1,
                name:'运营专员',
                des:'负责运营',
                count:12,
                node:[1,2,3,4,5,14,15,16]
            },
            {
                id:2,
                name:'订单管理员',
                des:'负责管理订单',
                count:60,
                node:[3,9,10,11,12,13,14]
            },
            {
                id:3,
                name:'超级管理员',
                des:'霸道总裁和程序员小娇妻',
                count:3,
                node:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
            },
            {
                id:4,
                name:'产品经理',
                des:'他是leader',
                count:5,
                node:[1,2,3,4,5,11,14,15,16]
            },
        ]
      const search = req.query.data
      rolesList = rolesList.filter(item => item.id == search || item.name == search)
        res.send({
            code:0,
            msg:'ok',
            data:{
                total:rolesList.length,
                rolesList
            }
        })
    })
    // 删除
    app.post('/api/role/delete',(req,res)=>{
        res.send({
            code:0,
            msg:'ok',
            data:1
        })
    })
    // 修改
    app.post('/api/role/change',(req,res)=>{
        res.send({
            code:0,
            msg:'ok',
            data:1
        })
    })
    // 新增
    app.post('/api/role/add',(req,res)=>{
        res.send({
            code:0,
            msg:'ok',
            data:1
        })
    })
}