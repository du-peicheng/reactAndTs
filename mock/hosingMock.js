module.exports=app=>{
    // 房源列表
    app.get('/api/hosing/list',(req,res)=>{
        const page = req.query.page || 1
        const pagesize = req.query.pagesize || 5
        let hosingTotal = []
        for(let i=1;i<=100;i++){
            hosingTotal.push({
                id:i,
                type:Math.random()>0.5 ? (Math.random()>0.5 ? '一室一厅' : '两室一厅') : (Math.random()>0.5 ? '三室一厅' : '两室两厅'),
                location:Math.random()>0.5 ?'汤臣一品' : '钓鱼台七号院',
                price:Math.floor(Math.random() * (5000 - 100 + 1) + 100),
                state:Math.random()>0.5? '已租' : '待租'
            })
        }
        hosingList=hosingTotal.slice((page-1)*pagesize,page*pagesize)
        res.send({
            code:0,
            msg:'ok',
            data:{
                total:hosingTotal.length,
                hosingList
            }
        })
    })
    // 搜索
    app.get('/api/hosing/search',(req,res)=>{
        const id = req.query.id || ''
        const location = req.query.location || ''
        const price = req.query.price || ''
        const type = req.query.type || ''
        let hosingTotal = []
        for(let i=1;i<=100;i++){
            hosingTotal.push({
                id:i,
                type:Math.random()>0.5 ? (Math.random()>0.5 ? '一室一厅' : '两室一厅') : (Math.random()>0.5 ? '三室一厅' : '两室两厅'),
                location:Math.random()>0.5 ?'汤臣一品' : '钓鱼台七号院',
                price:Math.floor(Math.random() * (5000 - 100 + 1) + 100),
                state:Math.random()>0.5? '已租' : '待租'
            })
        }
        hosingList = hosingTotal.filter(item=>{
            return (item.id == id || item.type == type) || (item.location == location || item.price == price)
        })
        
        res.send({
            code:0,
            msg:'ok',
            data:{
                total:hosingTotal.length,
                hosingList
            }
        })
    })
    // 上架下架
    app.post('/api/hosing/grounding',(req,res)=>{
        res.send({
            code:0,
            msg:'ok',
            data:1
        })
    })
    // 房源详情
    app.get('/api/hosing/detail',(req,res)=>{
        res.send({
            code:0,
            msg:'ok',
            data:{
                area:84,
                img:'https://picx.zhimg.com/70/v2-dd36cd60b42ced4e0fa1deaeebb8754d_1440w.avis?source=172ae18b&biz_tag=Post',
                date:['2023年8月21日','不限'],
                desc:'【交通出行】 园区附近有幼儿园、师范大学附属小学、师范附属中学、师范大学、航空航天大学、沈阳工程学院、辽宁大学、传媒大学等，教育资源丰富。【周边配套】 出行方便，有公交255、236、141、193、192、178等多路公交车，另有地铁二号线师范大学站。【小区介绍】 中海望京府住宅，正规商品房，民水民电，封闭式园区。人车分流，中海自持物业，安保24小时巡逻，每日保洁清扫。物业费含电梯费。',
                configuration:[2,3,4,5,6,7,8],
                name:'蔡徐坤',
                phone:13888888888
            }
        })
    })
}