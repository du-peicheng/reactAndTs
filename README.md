# react + typescript
## antd@5
> 从5版本后，它的组件样式使用了css-in-js技术，所以你引入组件时就相当于引入了css，从而就可以自动实现了css的按需引入
> 如果是4版本，则需要通过手动或webpack配置来实现自动按需引入样式