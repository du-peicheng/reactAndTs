declare namespace userType{
    export interface IUserinfo{
        id:number,
        name:string,
        token:string,
        avatar:string,
        roleid:number,
        time:number
    }
    export interface ILogin{
        username:string,
        password:string
    }
    // C端用户的数据
    export interface ICuser{
        id:number,
        name:string,
        sex:string,
        sculpture:number,
        type:string,
        state:number
    }

    // 用户列表
    export type UserList = IUserList[]

    export interface IUserList {
        id: number
        realName: string
        name: string
        phone: number
        roleid: number
        state: number
    }

    export interface IUserAdd{
        realName:string,
        name:string,
        phone:string|number,
        roleid:number,
    }
    
}