declare namespace roleType{
    export interface IRoleData {
        total: number
        rolesList: IRolesList[]
      }
      
      export interface IRolesList {
        id: number
        name: string
        des: string
        count: number
        node: number[]
      }
}