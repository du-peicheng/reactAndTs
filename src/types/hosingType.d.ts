declare namespace hosingType{
      export interface IHosingData {
        total: number
        hosingList: IHosingList[]
      }
      
      export interface IHosingList {
        id: number
        type: string
        location: string
        price: number
        state: string
      }
      export interface IHosingSearch{
        id:string
        loaction:string
        price:string
        type:string
      }
}