import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input,message } from 'antd';
import { userLoginAction } from '@/reducer/userinfo';
import { useAppDispatch,useAppSelector } from '@/store/hooks';
import { Navigate, useNavigate } from 'react-router-dom';
import { useState } from 'react';


const LoginForm = () => {
  const dispatch = useAppDispatch()
  const navigat = useNavigate()
  const [loading , setLoading ] = useState(false)
  const token = useAppSelector(state => state.userinfo.userinfo.token)
  if(token){
    return <Navigate to='/Cuser/list' replace/>
  }
    const onFinish = async (values:userType.ILogin) => {
      setLoading(true)
      let code = await dispatch(userLoginAction(values))
      if(code === 0){
        message.open({
          type:'success',
          duration:2,
          content:'登录成功',
          onClose:()=>{
            navigat('/Cuser/list',{replace:true})
          }
        })
      }else{
          message.open({
            type:'error',
            duration:2,
            content:'账号或密码错误',
            onClose:()=>{
              setLoading(false)
            }
          })
      }
      };    
    return (
        <div>
            <Form
      labelCol={{ span: 10 }}
      wrapperCol={{ span: 20 }}
      name="normal_login"
      className="login-form"
      initialValues={{ username:'kunkun',password:'372372' }}
      onFinish={onFinish}
    >
      <Form.Item
        name="username"
        rules={[{ required: true,whitespace:true, message: '请输入账号!' }]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="账号" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="密码"
        />
      </Form.Item>
      <Form.Item>
        
        <Button type="primary" htmlType="submit" className="login-form-button" loading={loading}>
          登录
        </Button>
      </Form.Item>
    </Form>
        </div>
    );
}

export default LoginForm;
