import React,{useEffect, useState} from 'react';
import CuserSearch from './ui/cusersearch';
import useApi from '@/hooks/useApi';
import { getCuserListApi,getSearchCuserApi } from '@/services/cuserApi';
import { Pagination  } from 'antd';
import './style.scss'
import CuserTable from './ui/cuserTable';
import config from '@/config/Duhome'
import { useSearchParams } from 'react-router-dom';

const Index = () => {
    const [loading,setLoading] = useState(false)
    const [searchparams,setSearchParams] = useSearchParams()
    const page = Number(searchparams.get('page')) || 1
    const pageSize = config.pageSize
    const [data,loadData] = useApi({},getCuserListApi)
    const [searchData,loadSearchData] = useApi({cusers:[]},getSearchCuserApi)
    const loadTableData = (page:number) => {
        loadData(page,pageSize)
    }
    useEffect(()=>{
        loadTableData(page)
    },[])
    return (
        <div className='cuser'>
            <CuserSearch  setSearchData={loadSearchData}/>
            <CuserTable loading={loading}  data={searchData.cusers.length >0 ?searchData : data} loadTableData={loadTableData}/>
             <Pagination defaultCurrent={Number(searchparams.get('page'))}  total={searchData.cusers.length >0 ?searchData.total : data.total} showTotal={(total) => `共 ${total}条数据`}  showSizeChanger={false}  onChange={page => {
            setLoading(true)
            loadData(page)
            setSearchParams({ page: page.toString() })
            setLoading(false)
          }}
          />
        </div>
    );
}

export default Index;
