import React, { FC,useState } from 'react';
import { Button, Table, Space,message,Modal } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import { useNavigate, useSearchParams } from 'react-router-dom';
import {changCuserStateApi} from '@/services/cuserApi'
interface IProps{
    loading:boolean
    data:{
        total:number
        cusers:userType.ICuser[]&{key:React.Key}[]
      }
      loadTableData:(page:number)=>void
}
const CuserTable:FC<IProps> = ({loading,data,loadTableData}) => {
    const navigate = useNavigate()
    const [isModalOpen, setIsModalOpen] = useState(false);
    
    const [showWhat,setShowWhat] = useState(0)
    const [userid,setUserid] = useState(0)
    const [searchParams,serSearchParams] = useSearchParams()
    const page = Number(searchParams.get('page')) || 1
  const showModal = (id:number,type:number) => {
    if(type===0){
        setShowWhat(0)
    }else{
        setShowWhat(1)
    }
    setIsModalOpen(true);
    setUserid(id)
  };
  const changeHand = async ()=>{
    const ret = await changCuserStateApi(userid)
    setIsModalOpen(false);
    if(ret.code === 0){
        message.success({
          content: '修改成功',
          duration: 3
        })
    }else{
        message.error({
          content: '修改失败',
          duration: 3
        })
    }
    loadTableData(page)
  }
  const handleOk = () => {
    changeHand()
  };
  
  const handleCancel = () => {
    setIsModalOpen(false);
  };

    const columns: ColumnsType<userType.ICuser&{key:React.Key}> = [
      {
        title: '序号',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '用户id',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '昵称',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '头像',
        key: 'sculpture',
        dataIndex: 'sculpture',
        render:(sculpture)=><img src={sculpture === 1 ? 'https://cdn7.axureshop.com/demo2023/2254049/images/c%E7%AB%AF%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86/u1286.svg' : ''} alt="" style={{width:'50px'}}/>
      },
      {
          title: '类型',
          key: 'type',
          dataIndex: 'type',
      },
      {
          title: '状态',
          key: 'state',
          dataIndex: 'state',
          render:(state)=>{
              
          return <span>{state==1 ? '启用' : '禁用'}</span>}
      },
      {
          title:'操作',
          key:'id',
          render:(data)=>{
          return <Space>
              <Button onClick={()=>{navigate('/Cuser/look',{state:{data}})}} type='primary'>查看</Button>
              <Button onClick={()=>{navigate('/Cuser/edit',{state:{data}})}} type='primary'>编辑</Button>
              {data.state == 1 ? <Button onClick={()=>showModal(data.id,1)} type='primary'>禁用</Button> : <Button onClick={()=>showModal(data.id,0)} type='primary'>启用</Button>}
          </Space>}
      }
    ];
    return (
        <>
        <Table loading={loading} className='table' columns={columns as ColumnsType<{ key: React.Key }>} bordered pagination={false} dataSource={data.cusers} rowKey={'id'} />
        <Modal keyboard={false} maskClosable={false}  title="" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <p>{ showWhat ? '禁用后，用户将无法使用该账户，是否禁用？' : '启用后，用户可以继续使用该账户，是否启用？'}</p>
      </Modal>
        
        </>
    );
}

export default CuserTable;
