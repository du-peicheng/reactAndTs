import React, { FC, useState } from 'react';
import { Button, Form, Input, Select, Space } from 'antd';
interface IProps{
  setSearchData:Function
}
const CuserSearch:FC<IProps> = ({setSearchData}) => {
    const [form] = Form.useForm();
    const [loading,setLoading] = useState(false)
    const onFinish=(value:{searchData:string})=>{
        setLoading(true)
        setSearchData(value.searchData)
        form.resetFields();
        setLoading(false)
    }
    return (
        <div className='Cuser-search'>
            <Form
            form={form}
            layout="inline"
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 24 }}
            name="normal_login"
            className="login-form"
            onFinish={onFinish}
    >
      <Form.Item
        name="searchData"
        rules={[{ required: true,whitespace:true, message: '请输入用户id或用户名' }]}
      >
        <Input className='search-input' placeholder="请输入用户id或用户名" />
      </Form.Item>
     
      <Form.Item>
        
        <Button type="primary" htmlType="submit" className="search-form-button" loading={loading}>
          搜索
        </Button>
        <Button
            onClick={() => {
              setSearchData({cusers:[]})
            }}
          >
            刷新
          </Button>
      </Form.Item>
    </Form>
        </div>
    );
}

export default CuserSearch;
