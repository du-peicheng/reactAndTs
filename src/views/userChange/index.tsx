import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './style.scss'
import { Button, Form, Input ,Select} from 'antd';
import { changeUserInfoApi} from '@/services/userApi'
import {isThatTrue} from '@/utils/tools'
const Index = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const [form] = Form.useForm()
    const subData = async (data:any)=>{
      const ret = await changeUserInfoApi(data)
      isThatTrue(ret.code,'修改用户信息')
    //   返回上一页
      window.history.back()
    }
    const onFinish = (values: any) => {
      subData(values)
      };
      
      const onFinishFailed = (errorInfo: any) => {
      };

    return (
      
      <div className='user-change'>
            <span className='look'>编辑</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>用户信息</span>
            </div>
        <Form
        form={form}
    wrapperCol={{ span: 10 }}
    labelWrap={true}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
    >
    <Form.Item
      label="用户姓名"
      name="realname" 
      rules={[{ required: true, message: '请输入真实姓名!' }]}
      initialValue={location.state.data.realName}
      >
      <Input placeholder="请输入用户姓名" allowClear/>
    </Form.Item>

    <Form.Item
      label="用户登录名"
      rules={[{ required: true, message: '用户登录名不能为空!' }]}
      name="name" 
      initialValue={location.state.data.name}
      >
      <Input placeholder="请输入用户登录名"  allowClear/>
    </Form.Item>
   
    <Form.Item
      label="手机号"
      className='cuserType'
      rules={[{ required: true, message: '请填写正确的手机号码!' ,pattern:/^1[3-9]\d{9}$/}]}
      name='phone'
      initialValue={location.state.data.phone}
      >
      <Input placeholder="请输入手机号"  allowClear/>
    </Form.Item>
    <Form.Item
      label="所属角色"
      name='roleid'
      rules={[{required:true,message:'请选择所属角色!'}]}
      initialValue={location.state.data.roleid}
      >
      <Select
      placeholder="请选择"
      style={{ width: 120 }}
      options={[
        { value: 1, label: '运营专员' },
        { value: 2, label: '订单管理员' },
        { value: 3, label: '超级管理员' ,disabled:true},
        { value: 4, label: '产品经理'},
      ]}
    />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button className='editBtn' type="primary" htmlType="submit">
        修改
      </Button>
      <Button className='goBackBtn' type="primary" onClick={()=>{navigate(-1)}}>
        取消
      </Button>
    </Form.Item>
  </Form>
      </div>
);
}

export default Index;
