import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './style.scss'
import { Form, Input,Checkbox,Button  } from 'antd';
import { changeRoleApi } from '@/services/roleApi'
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import { isThatTrue } from '@/utils/tools';
const Index = () => {
    const location = useLocation()
    const [form,setForm] = useState(location.state.data) 
    const [checkValue,setCheckValue] = useState(form.node)
    const navigate = useNavigate()
    const onFinish =async (values:any)=>{
        const data = {
            ...values,
            checkValue,
            id:form.id
        }
        const ret = await changeRoleApi(data)
        isThatTrue(ret.code)
        navigate(-1)
    }
    const onChange = (checkedValues: CheckboxValueType[]) => {
        setCheckValue(checkedValues)
      };
    const options = [
        { label: '新增', value: 1 },
        { label: '编辑', value: 2 },
        { label: '查看', value: 3 },
        { label: '启用', value: 4 },
        { label: '禁用', value: 5 },
        { label: '查看', value: 6 },
        { label: '处理', value: 7 },
        { label: '新增', value: 8 },
        { label: '新增', value: 9 },
        { label: '编辑', value: 10 },
        { label: '查看', value: 11 },
        { label: '启用', value: 12 },
        { label: '禁用', value: 13 },
      ];
    return (
        <div className='role-change'>
            <span className='look'>查看角色</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>角色信息</span>
            </div>
            <Form
            wrapperCol={{ span: 10 }}
            labelWrap={true}
            onFinish={onFinish}
            // defaultValue=
            >
                <Form.Item label='角色名称' name={'name'}
                rules={[{ required: true ,message: '角色名称不能为空！' }]}
                initialValue={form.name}
                >
                    <Input placeholder='请输入角色名称' />
                </Form.Item>
                <Form.Item label='角色描述'  name={'des'}
                rules={[{ required: true ,message: '角色描述不能为空！' }]}
                initialValue={form.des}
                >
                    <Input  placeholder='请输入角色描述' />
                </Form.Item>
                <Form.Item label='所属用户数量'  name={'count'}
                rules={[{ required: true,pattern: /^\d+$/ ,message: '角色数量为数字！' }]}
                initialValue={form.count}
                >
                    <Input placeholder='请输入所属用户数量'  />
                </Form.Item>
                
            <div className='bottom-title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>角色权限</span>
            </div>
            <table className='role-table'>
                <thead>
                    <tr>
                        <th>模块管理</th>
                        <th>功能权限</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>C端用户管理</td>
                        <td rowSpan={3}>
                        <Checkbox.Group className='role-checkBox' onChange={onChange}  options={options} defaultValue={form.node} />
                        </td>
                    </tr>
                    <tr>
                        <td>角色管理</td>
                    </tr>
                    <tr>
                        <td>后台用户管理</td>
                    </tr>
                </tbody>
            </table>
            <Form.Item>

                <Button className='goChangBtn' htmlType="submit" >保存</Button>
                <Button className='goBackBtn' onClick={()=>navigate(-1)}>取消</Button>
            </Form.Item>
            </Form>
        </div>
    );
}

export default Index;
