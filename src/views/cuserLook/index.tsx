import  { useState } from 'react';
import { useLocation } from 'react-router-dom';
import './style.scss'
import {  Form, Input } from 'antd';
const Index = () => {
    const location = useLocation()
    const [form,setForm] = useState(location.state.data) 
    return (
        <div className='cuser-detail'>
            <span className='look'>查看</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>用户信息</span>
            </div>
            <Form
            wrapperCol={{ span: 10 }}
            labelWrap={true}
            disabled={false}
            >
                <Form.Item label='昵称'>
                    <Input disabled readOnly value={form.name} />
                </Form.Item>
                <Form.Item label='用户ID'>
                    <Input disabled readOnly value={form.id} />
                </Form.Item>
                <Form.Item label='用户头像'>
                    <img src={form.sculpture ===1  ? 'https://cdn7.axureshop.com/demo2023/2254049/images/c%E7%AB%AF%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86/u1286.svg' : ''} style={{width:'50px'}} />
                </Form.Item>
                <Form.Item label='用户类型'>
                    <Input disabled readOnly value={form.type} />
                </Form.Item>
                <Form.Item label='状态'>
                    <Input disabled readOnly value={form.state=== 1 ? '启用' : '禁用'} />
                </Form.Item>
                <button className='goBackBtn' onClick={()=>window.history.back()}>返回</button>
            </Form>
        </div>
    );
}

export default Index;
