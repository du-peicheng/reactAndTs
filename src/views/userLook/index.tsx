import React, { useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import './style.scss'
import { Button, Form, Input, Select, Space } from 'antd';
const Index = () => {
    const location = useLocation()
    const [form,setForm] = useState(location.state.data) 
    const navigate = useNavigate()
    return (
        <div className='user-detail'>
            <span className='look'>查看</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>用户信息</span>
            </div>
            <Form
            wrapperCol={{ span: 10 }}
            labelWrap={true}
            disabled={false}
            >
                <Form.Item label='用户姓名'>
                    <Input disabled readOnly value={form.realName} />
                </Form.Item>
                <Form.Item label='登录名'>
                    <Input disabled readOnly value={form.name} />
                </Form.Item>
                <Form.Item label='手机号'>
                    <Input disabled readOnly value={form.phone} />
                </Form.Item>
                <Form.Item label='所属角色'>
                    <Input disabled readOnly value={form.roleid > 2 ? (form.roleid ===3 ? '超级管理员' : '产品经理') : (form.roleid ===1 ? '运营专员' : '订单管理员')} />
                </Form.Item>
                <button className='goBackBtn' onClick={()=>navigate(-1)}>返回</button>
            </Form>
        </div>
    );
}

export default Index;