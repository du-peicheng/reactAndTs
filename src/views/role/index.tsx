import React from 'react';
import useApi from '@/hooks/useApi';
import {getRoleListApi,searchRoleApi} from '@/services/roleApi'
import RoleTable from './ui/roleTable';
import RoleSearch from './ui/roleSearch';
import './style.scss'
const Index = () => {
    const [searchData,setSearchData] = useApi({rolesList:[]},searchRoleApi)
    const [data,loadData] = useApi({rolesList:[]},getRoleListApi)
    return (
        <div className='roleBox'>
            <RoleSearch setSearchData={setSearchData} />
            <RoleTable data={searchData.rolesList.length > 0 ?searchData : data}  loadTableData={loadData} />
        </div>
    );
}

export default Index;
