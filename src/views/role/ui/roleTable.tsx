import React, { FC,useState } from 'react';
import { Button, Table, Space,Modal } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import {deleteRoleApi} from '@/services/roleApi'
import config from '@/config/Duhome'
import {isThatTrue} from '@/utils/tools'
interface IProps{
    data:roleType.IRoleData
      loadTableData:(page:number)=>void
}



const RoleTable:FC<IProps> = ({data,loadTableData}) => {
  const [isModalOpen,setIsModalOpen] = useState(false)
    const [loading,setLoading] = useState(false)
    const [searchParams,setSearchParams] = useSearchParams()
    const paging = Number(searchParams.get('page')) || 1
    const pagingSize = config.pageSize
    const navigate = useNavigate()
    const [id,setId] = useState(1)
    const handleOk=async()=>{
      const ret = await deleteRoleApi(id)
      isThatTrue(ret.code,'删除')
      setIsModalOpen(false)
      loadTableData(paging)
    }
    const columns: ColumnsType<roleType.IRolesList&{key:React.Key}> = [
        {
          title: '序号',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: '角色名称',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: '角色描述',
          dataIndex: 'des',
          key: 'des',
        },
        {
            title: '用户数量',
            key: 'count',
            dataIndex: 'count',
        },
        {
            title:'操作',
            key:'id',
            render:(data)=>{
            return <Space>
                <Button onClick={()=>{navigate('/role/look',{state:{data}})}} type='primary'>查看</Button>
                <Button onClick={()=>{
                  setIsModalOpen(true)
                  setId(data.id)
                }} type='primary'>删除</Button>
                <Button onClick={()=>{navigate('/role/change',{state:{data}})}} type='primary'>编辑角色</Button>
            </Space>}
        }
      ];
    return (
        <>
            <Table loading={loading} className='table' pagination={{
                 position: ['bottomLeft'] ,
                 total: data.total,
                 showTotal: (total) => `共 ${total}条数据`,
                 showSizeChanger:false,
                 pageSize: pagingSize,
                 onChange(page=paging, pageSize=pagingSize) {
                    setLoading(true)
                    loadTableData(page)
                    setSearchParams({ page: page.toString() })
                    setLoading(false)
                 },
                 }} columns={columns as ColumnsType<roleType.IRolesList>} bordered  dataSource={data.rolesList} rowKey={'id'} />

            <Modal title="警告" open={isModalOpen} onOk={handleOk} onCancel={()=>setIsModalOpen(false)}>
                    <p>确认删除该角色？</p>
                  </Modal>
        </>
    );
}

export default RoleTable;
