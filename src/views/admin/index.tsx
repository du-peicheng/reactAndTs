import {useState} from 'react';
import { LaptopOutlined, NotificationOutlined, UserOutlined,PoweroffOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Breadcrumb, Layout, Menu, Modal  } from 'antd';
import { useAppDispatch,useAppSelector } from '@/store/hooks';
import { Navigate, Outlet, useLocation, useNavigate } from 'react-router-dom';
import store from '@/store';
import { clearUserInfo} from '@/reducer/userinfo'
import './style.scss'
const { Header, Content, Sider } = Layout;
type MeunItem = Required<MenuProps>['items'][number]

const menuItems:MeunItem[]=[
  {
    key:'/Cuser',
    icon:<LaptopOutlined />,
    label:'C端用户管理',
    // type:'group',
    children:[
      {
        key:'/Cuser/list',
        icon:<NotificationOutlined />,
        label:'C端用户列表',
      },
      // {
      //   key:'/Cuser/look',
      //   icon:<NotificationOutlined />,
      //   label:'查看',
      // },
    ]
  },
  {
    key:'/user',
    icon:<LaptopOutlined />,
    label:'后台用户管理',
    // type:'group',
    children:[
      {
        key:'/user/list',
        icon:<NotificationOutlined />,
        label:'后台用户列表',
      },
      // {
      //   key:'/user/add',
      //   icon:<NotificationOutlined />,
      //   label:'新增',
      // },
      // {
      //   key:'/user/look',
      //   icon:<NotificationOutlined />,
      //   label:'查看',
      // },
      // {
      //   key:'/user/change',
      //   icon:<NotificationOutlined />,
      //   label:'编辑',
      // }
    ]
  },
  {
    key:'/role',
    icon:<LaptopOutlined />,
    label:'角色管理',
    // type:'group',
    children:[
      {
        key:'/role/list',
        icon:<NotificationOutlined />,
        label:'角色列表',
      }
    ]
  },
  {
    key:'/hosing',
    icon:<LaptopOutlined />,
    label:'房源管理',
    // type:'group',
    children:[
      {
        key:'/hosing/list',
        icon:<NotificationOutlined />,
        label:'房源列表',
      }
    ]
  },
]

const Index = () => {

  const location = useLocation()
  const userinfo = useAppSelector(state => state.userinfo.userinfo)
  const [isModalOpen, setIsModalOpen] = useState(false);
  const token = userinfo.token
  const path=location.pathname
  const fatherPath = '/'+path.split('/')[1]
  const navigate = useNavigate()
  let breadTitle
  switch (fatherPath) {
    case '/Cuser':
      breadTitle='C端用户管理'
      break;
    case '/user':
      breadTitle='后台用户管理'
      break;
    case '/role':
      breadTitle='角色管理'
    break;
    case '/hosing':
      breadTitle='房源管理'
    break;
    default:
      break;
  }
  

  const showModal = () => {
    setIsModalOpen(true);
  };


  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // 判断是否非法进入本路由
  if(!token){
    return <Navigate to='/login' replace/>
  }
  // 判断进入时的路由
  if(path === '/'){
    return <Navigate to='/Cuser/list' replace/>
  }
  // 退出登录
const logoutHandle = ()=>{
  store.dispatch(clearUserInfo())
  window.location.href = '/login'
}
// 点击菜单项跳转路由/选中时跳转路由
const selectHandler=({key}:{key:string})=>{
  navigate(key)
}
// 面包屑用的
const findLabelByKey = (key:string, items:any):string => {
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    if (item.key === key) {
      return item.label as string;
    }
    if (item.children) {
      const label = findLabelByKey(key, item.children);
      if (label) {
        return label as string;
      }
    }
  }
  return '';
};
  return (
    <>
    <Layout className='duLayout'>
      <Header style={{ background:'#fff'}}  className='lay-head'>
        <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E7%99%BB%E5%BD%95_1/u1087.svg" alt="" />
        <span>xx租房平台后台管理系统</span>
        <div className='head-user'>
          <UserOutlined />
          <span className='name'>{userinfo.name}</span>
          <span className='logout' onClick={showModal}><PoweroffOutlined />退出</span>
        </div>
      </Header>
      <Layout>
        <Sider width={200}>
          <Menu
            mode="inline"
            defaultOpenKeys={[fatherPath]}
            defaultSelectedKeys={[path]}
            style={{ height: '100%', borderRight: 0 ,backgroundColor:'#3f5398',color:'#fff'}}
            items={menuItems}
            onClick={selectHandler}
            onSelect={selectHandler}
            />
        </Sider>
        <Layout style={{marginTop:10}}>
          
          <Content
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
              background:'#fff'
            }}
            >
          <Breadcrumb items={[
            {
              href:'',
              title:(
                <>
                  <UserOutlined />
                  {breadTitle}
                </>
              )
            },
            {
              href:'',
              title:(
                <>
                {findLabelByKey(path, menuItems)}
                </>
              )
            }
          ]}>
            
          </Breadcrumb>
            <Outlet/>
          </Content>
        </Layout>
      </Layout>
    </Layout>
    <Modal keyboard={false} maskClosable={false}  title="警告" open={isModalOpen} onOk={logoutHandle} onCancel={handleCancel}>
        <p>您确定要退出登录吗？</p>
      </Modal>
            </>
  );
};
export default Index
