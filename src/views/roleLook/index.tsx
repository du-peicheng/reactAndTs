import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './style.scss'
import { Form, Input,Checkbox  } from 'antd';
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
const Index = () => {
    const location = useLocation()
    const [form,setForm] = useState(location.state.data) 
    const navigate = useNavigate()
    const options = [
        { label: '新增', value: 1 },
        { label: '编辑', value: 2 },
        { label: '查看', value: 3 },
        { label: '启用', value: 4 },
        { label: '禁用', value: 5 },
        { label: '查看', value: 6 },
        { label: '处理', value: 7 },
        { label: '新增', value: 8 },
        { label: '新增', value: 9 },
        { label: '编辑', value: 10 },
        { label: '查看', value: 11 },
        { label: '启用', value: 12 },
        { label: '禁用', value: 13 },
      ];
    return (
        <div className='role-detail'>
            <span className='look'>查看角色</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>角色信息</span>
            </div>
            <Form
            wrapperCol={{ span: 10 }}
            labelWrap={true}
            disabled={false}
            >
                <Form.Item label='角色名称'>
                    <Input disabled readOnly value={form.name} />
                </Form.Item>
                <Form.Item label='角色描述'>
                    <Input disabled readOnly value={form.des} />
                </Form.Item>
                <Form.Item label='所属用户数量'>
                    <Input disabled readOnly value={form.count} />
                </Form.Item>
                
            </Form>
            <div className='bottom-title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>角色权限</span>
            </div>
            <table className='role-table'>
                <thead>
                    <tr>
                        <th>模块管理</th>
                        <th>功能权限</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>C端用户管理</td>
                        <td rowSpan={3}>
                        <Checkbox.Group className='role-checkBox' disabled options={options} defaultValue={form.node} />
                        </td>
                    </tr>
                    <tr>
                        <td>角色管理</td>
                    </tr>
                    <tr>
                        <td>后台用户管理</td>
                    </tr>
                </tbody>
            </table>
                <button className='goBackBtn' onClick={()=>navigate(-1)}>返回</button>
        </div>
    );
}

export default Index;
