import {  useNavigate } from 'react-router-dom';
import './style.scss'
import { Button, Form, Input ,Select} from 'antd';
import { addUserApi} from '@/services/userApi'
import {isThatTrue} from '@/utils/tools'
const Index = () => {
    const navigate = useNavigate()
    const [form] = Form.useForm()
    const subData = async (data:any)=>{
      const ret = await addUserApi(data)
      isThatTrue(ret.code,'添加用户')
      // 返回上一页
      window.history.back()
    }
    const onFinish = (values: any) => {
      subData(values)
      };
      
      const onFinishFailed = (errorInfo: any) => {
      };

    return (
      
      <div className='user-add'>
            <span className='look'>新增</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>用户信息</span>
            </div>
        <Form
        form={form}
    wrapperCol={{ span: 10 }}
    labelWrap={true}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
    >
    <Form.Item
      label="用户姓名"
      name="realname" 
      rules={[{ required: true, message: '请输入真实姓名!' }]}
      >
      <Input placeholder="请输入用户姓名"  allowClear/>
    </Form.Item>

    <Form.Item
      label="用户登录名"
      rules={[{ required: true, message: '用户登录名不能为空!' }]}
      name="name" 
      >
      <Input placeholder="请输入用户登录名"   allowClear/>
    </Form.Item>
   
    <Form.Item
      label="手机号"
      className='cuserType'
      rules={[{ required: true, message: '请填写正确的手机号码!' ,pattern:/^1[3-9]\d{9}$/}]}
      name='phone'
      >
      <Input placeholder="请输入手机号"  allowClear/>
    </Form.Item>
    <Form.Item
      label="所属角色"
      name='roleid'
      rules={[{required:true,message:'请选择所属角色!'}]}
      >
      <Select
      // defaultValue="运营专员"
      placeholder="请选择"
      style={{ width: 120 }}
      options={[
        { value: 1, label: '运营专员' },
        { value: 2, label: '订单管理员' },
        { value: 3, label: '超级管理员' ,disabled:true},
        { value: 4, label: '产品经理'},
      ]}
    />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <p>
        用户账号创建成功后，初始密码为000000
        </p>
      <Button className='editBtn' type="primary" htmlType="submit">
        新增
      </Button>
      <Button className='goBackBtn' type="primary" onClick={()=>{navigate(-1)}}>
        取消
      </Button>
    </Form.Item>
  </Form>
      </div>
);
}

export default Index;
