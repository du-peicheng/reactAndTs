import  { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './style.scss'
import { Button, Form, Input ,Upload, message} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { changCuserInfoApi} from '@/services/cuserApi'
const Index = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const [form] = Form.useForm()
    const subData = async (data:any)=>{
      const ret = await changCuserInfoApi(data)
      if(ret.code === 0){
        message.success({
          type:'success',
          content:'修改成功！'
        })
      }else{
        message.error({
          type:'error',
          content:'修改失败！'
        })
      }
      // 返回上一页
      window.history.back()
    }
    const onFinish = (values: any) => {
      const data = {
        ...values,
        name:form.getFieldValue('name'),
        id:location.state.data.id
      }
      subData(data)
      };
      
      const onFinishFailed = (errorInfo: any) => {
      };
      const normFile = (e: any) => {
        if (Array.isArray(e)) {
          return e;
        }
        return e?.fileList;
      };
      useEffect(()=>{
        form.setFieldsValue(location.state.data)
      },[])
    return (
      
      <div className='cuser-edit'>
            <span className='look'>编辑</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>用户信息</span>
            </div>
        <Form
        form={form}
    wrapperCol={{ span: 10 }}
    labelWrap={true}
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}
    autoComplete="off"
    >
    <Form.Item
      label="昵称"
      
      rules={[{ required: true, message: '请输入昵称!' }]}
      >
      <Input  value={location.state.data.name} allowClear/>
    </Form.Item>

    <Form.Item
      label="用户id"
      
      >
      <Input value={location.state.data.id} disabled />
    </Form.Item>
    
    <Form.Item label="头像" className='sculpture'>
    <Form.Item name="dragger" valuePropName="fileList" getValueFromEvent={normFile} noStyle>
        <Upload.Dragger name="files" action="/upload.do">
          <p className="ant-upload-drag-icon">
          <PlusOutlined />
          </p>
          <p className="ant-upload-text">点击上传头像</p>
        </Upload.Dragger>
      </Form.Item>
    </Form.Item>
    <Form.Item
      label="用户类型"
      className='cuserType'
      >
      <Input value={location.state.data.type} disabled />
    </Form.Item>
    <Form.Item
      label="用户状态"
      
      >
      <Input value={location.state.data.state === 1 ? '启用' : '禁用'} disabled />
    </Form.Item>

    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
      <Button className='editBtn' type="primary" htmlType="submit">
        修改
      </Button>
      <Button className='goBackBtn' type="primary" onClick={()=>{navigate(-1)}}>
        取消
      </Button>
    </Form.Item>
  </Form>
      </div>
);
}

export default Index;
