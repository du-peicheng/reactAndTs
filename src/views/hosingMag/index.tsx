import React, { useState } from 'react';
import HosingSearch from './ui/hosingSearch';
import HosingTable from './ui/hosingTable'
import {getHosingListApi} from '@/services/hosingApi'
import './style.scss'
import useApi from '@/hooks/useApi';
const Index = () => {
    const [searchData,setSearchData] = useState({hosingList:[]})
    const [data,loadData] = useApi({hosingList:[]},getHosingListApi)
    return (
        <div className='hosing-box'>
            <HosingSearch setSearchData={setSearchData}/>
            <HosingTable  data={searchData.hosingList.length>0 ? searchData : data} loadTableData = {loadData}/>
        </div>
    );
}

export default Index;
