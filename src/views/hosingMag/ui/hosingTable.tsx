import React, { FC,useState } from 'react';
import { Button, Table, Space,Modal } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import {hosingGroundingApi} from '@/services/hosingApi'
import config from '@/config/Duhome'
import {isThatTrue} from '@/utils/tools'

interface IProps{
    data:hosingType.IHosingData
    loadTableData:(page:number)=>void
} 
const HosingTable:FC<IProps> = ({data,loadTableData}) => {

    const navigate = useNavigate()
    const [isModalOpen,setIsModalOpen] = useState(false)
    const [loading,setLoading] = useState(false)
    const [showWhat,setShowWhat] = useState(false)
    const [searchParams, setSearchParams] = useSearchParams()
    const paging = Number(searchParams.get('page')) || 1
    const pagingSize = config.pageSize
    const [id,setId] = useState(0)

    const columns: ColumnsType<hosingType.IHosingList&{key:React.Key}> = [
        {
          title: '序号',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: '房源编号',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: '户型',
          dataIndex: 'type',
          key: 'type',
        },
        {
            title: '位置',
            key: 'location',
            dataIndex: 'location',
        },
        {
            title: '价格',
            key: 'price',
            dataIndex: 'price',
        },
        {
            title: '状态',
            key: 'state',
            dataIndex: 'state',
            render:(state)=>{
                return (
                    <span className={state === '待租' ? 'waitLease' : 'finishLease'}>
                        {state}
                    </span>
                )
            }
        },
        {
            title:'操作',
            key:'id',
            render:(data)=>{
            return <Space>
                <Button onClick={()=>{navigate('/hosing/look',{state:{data}})}} type='primary'>查看</Button>
                <Button onClick={()=>{
                setShowWhat(data.state === '待租' ? true : false)
                setId(data.id)
                setIsModalOpen(true)
                }} type='primary'>{data.state === '待租' ? '下架' : '重新上架'}</Button>
            </Space>}
        }
      ];
      const handleOk = async()=>{
        const ret = await hosingGroundingApi(id)
        isThatTrue(ret.code)
        setIsModalOpen(false)
      }
    return (
        <>
             <Table loading={loading} className='table' pagination={{
                 position: ['bottomLeft'] ,
                 total: data.total,
                 showTotal: (total) => `共 ${total}条数据`,
                 showSizeChanger:false,
                 pageSize: pagingSize,
                 onChange(page=paging, pageSize=pagingSize) {
                    setLoading(true)
                    loadTableData(page)
                    setSearchParams({ page: page.toString() })
                    setLoading(false)
                 },
                 }} columns={columns as ColumnsType<hosingType.IHosingList>} bordered  dataSource={data.hosingList} rowKey={'id'} />
                <Modal title="" open={isModalOpen} onOk={handleOk} onCancel={()=>setIsModalOpen(false)}>
                    <p>{showWhat ? '确认下架该房源？' : '重新上架该房源？'}</p>
                  </Modal>
        </>
    );
}

export default HosingTable;
