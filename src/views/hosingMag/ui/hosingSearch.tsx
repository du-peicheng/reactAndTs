import React, { FC, useState } from 'react';
import { Button, Form, Input, Select, Space, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import {searchHosingListApi} from '@/services/hosingApi'
interface IProps{
  setSearchData:Function
}
const HosingSearch:FC<IProps> = ({setSearchData}) => {
    const navigate = useNavigate()
    const [form] = Form.useForm();
    const [loading,setLoading] = useState(false)
    const onFinish=async (value:hosingType.IHosingSearch)=>{
        setLoading(true)
        const ret = await searchHosingListApi(value.id,value.loaction,value.price,value.type)
        if(ret.code === 0){
            setSearchData(ret.data)
        }else{
            message.error({
                content: `搜索出错了！`,
                duration: 3
              })
        }
        setLoading(false)
    }
    return (
        <div className='role-search'>
            <Form
            form={form}
            layout="inline"
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 24 }}
            name="normal_login"
            className="login-form"
            onFinish={onFinish}
    >
      <Form.Item
        name="id"
      >
        <Input className='search-input' placeholder="请输入房源编号" />
      </Form.Item>
      <Form.Item
        name="loaction"
      >
        <Input className='search-input' placeholder="请输入房源位置" />
      </Form.Item>
      <Form.Item
        name="price"
      >
        <Select className='search-select' placeholder="请选择价格区间" options={[
        { value:'请选择价格区间' , label: '请选择区间（元）' ,disabled:true},
        { value:'1000元以下' , label: '1000元以下' },
        { value: '1000-2000', label: '1000-2000' },
        { value: '2000-3500', label: '2000-3500' },
        { value: '3500以上', label: '3500以上'},
      ]}/>
      </Form.Item>
      <Form.Item
        name="type"
      >
        <Select className='search-select' placeholder="请选择户型" options={[
        { value:'请选择户型' , label: '请选择户型' ,disabled:true},
        { value:'一室一厅' , label: '一室一厅' },
        { value: '两室一厅', label: '两室一厅' },
        { value: '两室两厅', label: '两室两厅' },
        { value: '三室一厅', label: '三室一厅'},
      ]}/>
      </Form.Item>
     
      <Form.Item>
        
        <Button type="primary" htmlType="submit" className="search-form-button" loading={loading}>
          搜索
        </Button>
        <Button
            className='reload-btn'
            onClick={() => {
              setSearchData({hosingList:[]})
            }}
          >
            刷新
          </Button>
      </Form.Item>
    </Form>
        </div>
    );
}

export default HosingSearch;
