import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './style.scss'
import { Form, Input,Checkbox,Space  } from 'antd';
import {hosingDetailApi} from '@/services/hosingApi'
import type { CheckboxValueType } from 'antd/es/checkbox/Group';
import useApi from '@/hooks/useApi';
const Index = () => {
    const location = useLocation()
    const [form,setForm] = useState(location.state.data) 
    const [data,loadData] = useApi({},hosingDetailApi)
    const text = data.desc ? data.desc.split(/[。.]/) : ''
    const date = data.date ?  data.date[0]+','+data.date[1] : ''
    const navigate = useNavigate()
    const options = [
        { label: '电视', value: 1 },
        { label: '洗衣机', value: 2 },
        { label: '冰箱', value: 3 },
        { label: '空调', value: 4 },
        { label: '床', value: 5 },
        { label: 'wifi', value: 6 },
        { label: '沙发', value: 7 },
        { label: '热水器', value: 8 },
      ];
    return (
        <div className='hosing-detail'>
            <span className='look'>查看</span>
            <div className='title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>房源信息</span>
            </div>
            <Form
            className='hosing-form'
            wrapperCol={{ span: 6 }}
            labelWrap={true}
            disabled={false}
            >
                <Form.Item label='房源编号'>
                    <Input disabled readOnly value={form.id} />
                </Form.Item>
                <Form.Item label='户型'>
                    <Input disabled readOnly value={form.type} />
                </Form.Item>
                <Form.Item label='面积'>
                    <Input disabled readOnly value={data.area}  addonAfter="㎡"/>
                </Form.Item>
                <Form.Item label='租金（元）'>
                    <Input disabled readOnly value={form.price} />
                </Form.Item>
                <Form.Item label='位置'>
                    <Input disabled readOnly value={form.location} />
                </Form.Item>
                <Form.Item label='房源照片'>
                    <img className='hosing-img' src={data.img} alt="" />
                </Form.Item>

                <Form.Item label='租期' className='date'>
                    <Input disabled readOnly value={date} />
                </Form.Item>
                <Form.Item label='房源描述' className='desc'>
                    <div className='desc-box'>
                    <p>{text === '' ? text : text[0]}</p>
                    <p>{text === '' ? text : text[1]}</p>
                    <p>{text === '' ? text : text[2]}</p>
                    <p>{text === '' ? text : text[3]}</p>
                    <p>{text === '' ? text : text[4]}</p>
                    <p>{text === '' ? text : text[5]}</p>
                    </div>
                </Form.Item>
                <Form.Item label='出租日期与租期' className='hosing-checkBox'>
                <Checkbox.Group className='role-checkBox' disabled options={options} value={data.configuration} />
                </Form.Item>
            <Form.Item label='联系人昵称' className='hosing-name'>
                <Input disabled readOnly value={data.name} />
            </Form.Item>
            <Form.Item label='联系电话' className='hosing-phone'>
                <Input disabled readOnly value={data.phone} />
            </Form.Item>
            </Form>

            {/* <div className='bottom-title'>
                <img src="https://cdn7.axureshop.com/demo2023/2254049/images/%E6%9F%A5%E7%9C%8B/u1361.svg" alt="" />
                <span>角色权限</span>
            </div>
            <table className='role-table'>
                <thead>
                    <tr>
                        <th>模块管理</th>
                        <th>功能权限</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>C端用户管理</td>
                        <td rowSpan={3}>
                        
                        </td>
                    </tr>
                    <tr>
                        <td>角色管理</td>
                    </tr>
                    <tr>
                        <td>后台用户管理</td>
                    </tr>
                </tbody>
            </table> */}
                <button className='goBackBtn' onClick={()=>navigate(-1)}>返回</button>
        </div>
    );
}

export default Index;
