import React, { FC, useState } from 'react';
import { Button, Form, Input, Select, Space } from 'antd';
import { useNavigate } from 'react-router-dom';
interface IProps{
  setSearchData:Function
}
const UserSearch:FC<IProps> = ({setSearchData}) => {
    const navigate = useNavigate()
    const [form] = Form.useForm();
    const [loading,setLoading] = useState(false)
    const onFinish=(value:{searchData:string})=>{
        setLoading(true)
        setSearchData(value.searchData)
        form.resetFields();
        setLoading(false)
    }
    return (
        <div className='user-search'>
            <Form
            form={form}
            layout="inline"
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 24 }}
            name="normal_login"
            className="login-form"
            onFinish={onFinish}
    >
      <Form.Item
        name="searchData"
        rules={[{ required: true,whitespace:true, message: '请输入用户id或用户名' }]}
      >
        <Input className='search-input' placeholder="请输入用户id或用户名" />
      </Form.Item>
     
      <Form.Item>
        
        <Button type="primary" htmlType="submit" className="search-form-button" loading={loading}>
          搜索
        </Button>
        <Button
            className='reload-btn'
            onClick={() => {
              setSearchData({cusers:[]})
            }}
          >
            刷新
          </Button>
        <Button
            onClick={() => {
                navigate('/user/add')
            }}
          >
            新增
          </Button>
      </Form.Item>
    </Form>
        </div>
    );
}

export default UserSearch;
