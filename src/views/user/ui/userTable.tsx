import React, { FC,useState } from 'react';
import { Button, Table, Space,message,Modal } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import {changeUserStateApi,resetpasswordApi} from '@/services/userApi'
import config from '@/config/Duhome'
import {isThatTrue} from '@/utils/tools'
interface IProps{
    data:{
        total:number
        users:userType.UserList&{key:React.Key}[]
      }
      loadTableData:(page:number)=>void
}

const UserTable:FC<IProps> = ({data,loadTableData}) => {
    const navigate = useNavigate()
    const [searchparams,setSearchParams] = useSearchParams()
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isreset, setIsreset] = useState(false);
    const [showWhat,setShowWhat] = useState(0)
    const [resetData,setResetData] = useState({} as userType.IUserList)
    const [loading,setLoading] = useState(false)
    const pagingSize = config.pageSize
    const [userid,setUserid] = useState(0)
    const [paging,setPaging] = useState(Number(searchparams.get('page')) || 1)
    const showModal = (id:number,type:number) => {
        if(type===0){
            setShowWhat(0)
        }else{
            setShowWhat(1)
        }
        setIsModalOpen(true);
        setUserid(id)
      };
    const changeStateHandle = async ()=>{
        const ret = await changeUserStateApi(userid)
    setIsModalOpen(false);
    isThatTrue(ret.code)
    loadTableData(paging)
    }
    // 重置密码
    const resetOk =async ()=>{
            const ret = await resetpasswordApi(resetData.id)
            isThatTrue(ret.code,'重置')
            setIsreset(false)        
    }

    const showReset=(data:userType.IUserList&{key:React.Key})=>{
        setIsreset(true)
        setResetData(data)
    }
    
    const columns: ColumnsType<userType.IUserList&{key:React.Key}> = [
        {
          title: '序号',
          dataIndex: 'id',
          key: 'id',
        },
        {
          title: '用户姓名',
          dataIndex: 'realName',
          key: 'realName',
        },
        {
          title: '登录名',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: '手机号',
          key: 'phone',
          dataIndex: 'phone',
          render:(phone)=><span>{String(phone).replace(/(\d{3})\d+(\d{4})/,'$1****$2')}</span>
        },
        {
            title: '状态',
            key: 'state',
            dataIndex: 'state',
            render:(state)=>{  
            return <span>{state==1 ? '启用' : '禁用'}</span>}
        },
        {
            title:'操作',
            key:'id',
            render:(data)=>{
            return <Space>
                <Button onClick={()=>{navigate('/user/look',{state:{data}})}} type='primary'>查看</Button>
                <Button onClick={()=>{navigate('/user/change',{state:{data}})}} type='primary'>编辑</Button>
                <Button onClick={()=>showReset(data)} type='primary'>重置密码</Button>
                {data.state == 1 ? <Button onClick={()=>showModal(data.id,1)} type='primary'>禁用</Button> : <Button onClick={()=>showModal(data.id,0)} type='primary'>启用</Button>}
            </Space>}
        }
      ];
    return (
        <div className='user-table'>
            <Table loading={loading} className='table' pagination={{
                 position: ['bottomLeft'] ,
                 total: data.total,
                 showTotal: (total) => `共 ${total}条数据`,
                 showSizeChanger:false,
                 onChange(page=paging, pageSize=pagingSize) {
                    setLoading(true)
                    loadTableData(page)
                    setSearchParams({ page: page.toString() })
                    setLoading(false)
                 },
                 }} columns={columns as ColumnsType<{ key: React.Key }>} bordered  dataSource={data.users} rowKey={'id'} />
        <Modal keyboard={false} maskClosable={false} title="" open={isModalOpen} onOk={changeStateHandle} onCancel={()=>{setIsModalOpen(false);}}>
        <p>{ showWhat ? '禁用后，用户将无法使用该账户，是否禁用？' : '启用后，用户可以继续使用该账户，是否启用？'}</p>
      </Modal>
      <Modal className='reset-box' keyboard={false} maskClosable={false}  title="" open={isreset} onOk={resetOk} onCancel={()=>{setIsreset(false);}}>
        <p className='reset'>重置密码</p>
        <div className='reset-item'>
            <span className='reset-item-first'>用户姓名:</span>
            <span className='reset-item-second'>{resetData.realName}</span>
        </div>
        <div className='reset-item'>
            <span className='reset-item-first'>手机号:</span>
            <span className='reset-item-second'>{String(resetData.phone).replace(/(\d{3})\d+(\d{4})/,'$1****$2')}</span>
        </div>
        <div className='reset-item'>
            <span className='reset-item-first'>登录名:</span>
            <span className='reset-item-second'>{resetData.name}</span>
        </div>
        <p className='reset-mes'>用户重置密码后，密码变为000000</p>
      </Modal>
        </div>
    );
}

export default UserTable;
