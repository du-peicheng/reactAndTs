import useApi from '@/hooks/useApi';
import {getUserListApi,searchUserApi} from '@/services/userApi'
import {useEffect} from 'react';
import UserSearch from './ui/userSearch';
import './style.scss'
import UserTable from './ui/userTable';

const Index = () => {
    const [ searchData,setSearchData] = useApi({users:[]},searchUserApi)
    const [data,loadData] = useApi({users:[]},getUserListApi)
    const loadTableData = (page:number) => {
        loadData(page,10)
    }
    useEffect(()=>{
        loadTableData(1)
    },[])
    return (
        <div>
            <UserSearch  setSearchData={setSearchData}/>
            <UserTable data={searchData.users.length>0 ? searchData : data} loadTableData={loadData}/>
        </div>
    );
}

export default Index;
