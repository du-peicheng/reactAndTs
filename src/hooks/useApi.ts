import { useEffect,useState } from "react";

const useApi = (initData:any,api:Function)=>{
    const [data,setData] = useState(initData)
    const loadData = async (data:any='')=>{
        const ret = await api(data)
        if(ret.code===0){
            setData(ret.data)
        }
    }
    useEffect(()=>{
        loadData()
    },[])
    return [data,loadData]
}
export default useApi