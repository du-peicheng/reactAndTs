import { Suspense, lazy } from 'react'
import { Outlet, type RouteObject } from 'react-router-dom'
import { Spin } from 'antd'

const Cuser = lazy(() => import('@/views/cuser'))
const CuserLook = lazy(() => import('@/views/cuserLook'))
const User = lazy(()=>import('@/views/user'))
const Useradd = lazy(()=>import('@/views/userAdd'))
const Userlook = lazy(()=>import('@/views/userLook'))
const Userchange = lazy(()=>import('@/views/userChange'))
const CuserEdit = lazy(()=>import('@/views/cuserEdit'))
const Role = lazy(()=>import('@/views/role'))
const RoleLook = lazy(()=>import('@/views/roleLook'))
const RoleChange = lazy(()=>import('@/views/roleChange'))
const RoleAdd = lazy(()=>import('@/views/roleAdd'))
const HosingMag = lazy(()=>import('@/views/hosingMag'))
const HosingLook = lazy(()=>import('@/views/hosingLook'))
// const routes: RouteObject[] = [
//   {
//     path: '/info/dashboard',
//     element: (
//       <Suspense fallback={<Spin size='large' />}>
//         <Dashboard />
//       </Suspense>
//     )
//   }
// ]
const routes: RouteObject[] = [
  {
    path: 'Cuser',
    element: <Outlet />,
    children: [
      {
        index:true,
        path: 'list',
        element: (
          <Suspense fallback={<Spin size='large' />}>
            <Cuser />
          </Suspense>
        )
      },
      {
        path:'look',
        element:(
          <Suspense>
            <CuserLook/>
          </Suspense>
        )
      },
      {
        path:'edit',
        element:(
          <Suspense>
            <CuserEdit/>
          </Suspense>
        )
      },
    ]
  },
  {
    path: 'user',
    element: <Outlet />,
    children: [
      {
        index:true,
        path: 'list',
        element: (
          <Suspense fallback={<Spin size='large' />}>
            <User />
          </Suspense>
        )
      },
      {
        path:'add',
        element:(
          <Suspense>
            <Useradd/>
          </Suspense>
        )
      },
      {
        path:'look',
        element:(
          <Suspense>
            <Userlook/>
          </Suspense>
        )
      },
      {
        path:'change',
        element:(
          <Suspense>
            <Userchange/>
          </Suspense>
        )
      }
    ]
  },
  {
    path:'role',
    element:<Outlet />,
    children:[
      {
        index:true,
        path:'list',
        element:(
          <Suspense>
            <Role />
          </Suspense>
        )
      },
      {
        path:'look',
        element:(
          <Suspense>
            <RoleLook/>
          </Suspense>
        )
      },
      {
        path:'change',
        element:(
          <Suspense>
            <RoleChange/>
          </Suspense>
        )
      },
      {
        path:'add',
        element:(
          <Suspense>
            <RoleAdd/>
          </Suspense>
        )
      }
    ]
  },{
    path:'hosing',
    element:<Outlet />,
    children:[
      {
        path:'list',
        element:(
          <Suspense>
            <HosingMag />
          </Suspense>
        )
      },
      {
        path:'look',
        element:(
          <Suspense>
            <HosingLook/>
          </Suspense>
        )
      }
    ]
  }
]

export default routes
