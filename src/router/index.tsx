import { createBrowserRouter } from 'react-router-dom'
import type { RouteObject } from 'react-router-dom'
import Login from '@/views/login'
import Admin from '@/views/admin'
// 自动引入路由
const moduleFn = (require as any).context('./routes', false, /\.tsx$/)
const children = moduleFn.keys().reduce((modules: RouteObject[], curr: string) => {
  const value = moduleFn(curr).default
  if (Array.isArray(value)) {
    modules.push(...value)
  } else {
    modules.push(value)
  }
  return modules
}, [])
const router = createBrowserRouter([
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/',
    element: <Admin />,
    children
  },
  {
    path: '*',
    element: <div>404</div>
  }
])

export default router
