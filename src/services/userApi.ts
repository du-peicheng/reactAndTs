import {get,post} from '@/utils/http'

// 登录
export const loginApi =(data:userType.ILogin)=> post('/api/user/login',data) 

// 获取用户列表
export const getUserListApi = (page:number) =>get<userType.IUserList>(`/api/user/list?page=${page}`)

// 用户列表搜索功能
export const searchUserApi = (searchData:string) =>get<userType.IUserList>(`/api/user/search?data=${searchData}`)

// 修改用户状态
export const changeUserStateApi=(id:number)=>post(`/api/user/changestate`,id)


// 重置用户密码
export const resetpasswordApi=(id:number)=>post(`/api/user/resetpassword`,id)


// 新增后台用户
export const addUserApi=(data:userType.IUserAdd)=>{
    data.phone=Number(data.phone)
    return post(`/api/user/adduser`,data)
}


// 修改用户信息
export const changeUserInfoApi=(data:userType.IUserAdd)=>{
    data.phone=Number(data.phone)
    return post(`/api/user/changeUser`,data)
}