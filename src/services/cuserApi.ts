import {get,post} from '@/utils/http'

// 获取C端用户列表

export const getCuserListApi=(page:number=1,pagesize:number=10)=>get(`/api/cuser/listdata?page=${page}&pagesize=${pagesize}`)

// 获取C端用户搜索

export const getSearchCuserApi=(data:string)=>get(`/api/cuser/searchData?data=${data}`)

// 修改C端用户状态
export const changCuserStateApi=(id:number)=>post(`/api/cuser/changestate`,id)

// 修改C端用户信息
export const changCuserInfoApi=(data:any)=>post(`/api/cuser/changinfo`,data)