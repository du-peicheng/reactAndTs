import {get,post} from '@/utils/http'
import config from '@/config/Duhome'
// 列表
export const getHosingListApi = (page:number=1,pagesize:number=config.pageSize) =>get(`/api/hosing/list?page=${page}&pagesize=${pagesize}`)


// 搜索
export const searchHosingListApi = (id:string='',loaction:string='',price:string='',type:string='') =>get(`/api/hosing/search?id=${id}&location=${loaction}$price=${price}&type=${type}`)

// 上架下架
export const hosingGroundingApi = (id:number) => post('/api/hosing/grounding',id)


// 房源详情
export const hosingDetailApi = (id:number) => get(`/api/hosing/detail?id=${id}`)