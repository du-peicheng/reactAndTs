import {get,post} from '@/utils/http'

// 获取角色列表
export const getRoleListApi = (page:number=1,pagesize:number=10)=>get(`/api/role/list?page=${page}&pagesize=${pagesize}`)


// 搜索
export const searchRoleApi = (searchData:string) =>get(`/api/role/searchList?data=${searchData}`)

// 删除
export const deleteRoleApi = (id:number)=>post('/api/role/delete',id) 


// 修改
export const changeRoleApi = (data:roleType.IRolesList)=>post('/api/role/change',data) 


// 新增
export const addRoleApi = (data:Omit<roleType.IRolesList,'id'|'count'>)=>post('/api/role/add',data) 