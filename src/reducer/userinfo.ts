import { createSlice } from '@reduxjs/toolkit';
import cache from '@/utils/cache'
import { AppThunk } from '@/store';
import { loginApi } from '@/services/userApi'
const DoUserInfo = createSlice({
  name:'userinfo',
  initialState: {
    userinfo:{
    name:'',
    token:'',
    id:0,
    avatar:'',
    roleid:0,
    time:0
  }
  },
  reducers: {
    setUserInfo: (state,{payload}:{payload:userType.IUserinfo}) => {
      state.userinfo=payload;
      cache().set('userinfo',payload);
    },
    clearUserInfo(state){
      state.userinfo={
        id:0,
        roleid:0,
        name:'',
        token:'',
        avatar:'',
        time:0
      }
      cache().remove('userinfo');
    }
  }
});

export const { setUserInfo,clearUserInfo } = DoUserInfo.actions;
// 异步中间件--用户登录
export const userLoginAction = (data:userType.ILogin):AppThunk<Promise<number>>=>
  async dispatch =>{
    let ret = await loginApi(data);
    if(ret.code === 0){
      dispatch(setUserInfo(ret.data as userType.IUserinfo));
    }
    return ret.code
  }


export default DoUserInfo.reducer;