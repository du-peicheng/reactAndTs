import React from 'react'
import ReactDOM from 'react-dom/client'

// 重置样式
import './style/reset.scss'

// 路由
import { RouterProvider } from 'react-router-dom'
import router from './router'

// redux
import { Provider } from 'react-redux'
import store from './store'
import './store/redux-presist'

// antd本地化处理
import { ConfigProvider } from 'antd'
import zhCN from 'antd/locale/zh_CN'
// 日期处理本地化
import dayjs from 'dayjs'
import 'dayjs/locale/zh-cn'

// 日期全局使用中文
dayjs.locale('zh-cn')

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
  <Provider store={store}>
    <ConfigProvider locale={zhCN}>
      <RouterProvider router={router} />
    </ConfigProvider>
  </Provider>
)
