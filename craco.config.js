// npm i -D @craco/craco
const path = require('path')
const userMock  = require('./mock/userMock.js')
const cuserMock = require('./mock/cuserMock.js')
const roleMock = require('./mock/roleMock.js')
const hosingMock = require('./mock/hosingMock.js')
module.exports = {
  webpack: {
    alias: {
      // 将 @ 别名指向你的源代码目录
      '@': path.resolve('src')
    }
  },
  babel: {
    plugins: []
  },
  // style: {
  //   postcss: {
  //     loaderOptions: {
  //       postcssOptions: {
  //         ident: 'postcss',
  //         plugins: [
  //           [
  //             // yarn add -D postcss-pxtorem@6
  //             'postcss-pxtorem',
  //             {
  //               rootValue: 37.5, // 根元素字体大小
  //               unitPrecision: 6, // 小数点后保留的位数
  //               // 所有的属性中只要有px，都给转成rem，除style内联和Px或PX 以外
  //               propList: ['*']
  //             }
  //           ]
  //         ]
  //       }
  //     }
  //   }
  // },
  devServer: {
    setupMiddlewares: (middlewares, { app }) => {
      userMock(app)
      cuserMock(app)
      roleMock(app)
      hosingMock(app)
      return middlewares
    }
  }
}
